package main.beuthcity.buildings;

import javafx.scene.Group;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;
import main.beuthcity.buildings.types.*;
import main.beuthcity.player.Player;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

public class BuildingMap {
    private final TilePane root;
    private final Player player;

    private Map<Type, BuildingInterface> buildingMap = new HashMap<>();
    private BuildingInterface activeBuilding;

    public BuildingMap(TilePane root, Player player) {
        this.root = root;
        this.player = player;

        this.setBuildingMap();
    }

    public void setActiveBuilding(BuildingInterface buildingInterface) {
        if(buildingInterface != null) {
            this.activeBuilding = buildingInterface;
            this.activeBuilding.getImageWrapper().getStyleClass().add("active");
        } else {
            this.activeBuilding.getImageWrapper().getStyleClass().remove("active");
            this.activeBuilding = null;
        }
    }

    public BuildingInterface getActiveBuilding() {
        return this.activeBuilding;
    }

    private void setBuildingMap() {
        // Factory-Pattern
        this.generate(new House());
        this.generate(new Lodge());
        this.generate(new Loft());
        this.generate(new Tower());
        this.generate(new SkyScraper());
    }

    public BuildingInterface getBuildingByType(Type type) {
        if(type == Type.NONE) {
            return null;
        }

        return this.buildingMap.get(type);
    }

    private void generate(BuildingInterface buildingInterface) {
        try {
            Pane imageWrapper = new Pane(buildingInterface.getImageView());
            imageWrapper.getStyleClass().add("buildingWrapper");

            Tooltip tooltip = new Tooltip(this.tooltip(buildingInterface));
            Tooltip.install(imageWrapper, tooltip);

            Group group = new Group(imageWrapper);
            group.getStyleClass().add("building");
            group.onMouseClickedProperty().set(event -> this.assignClickedBuildingToActive(event.getSource()));

            buildingInterface.setGroup(group);

            this.updateBuildingStatus(buildingInterface);
            this.root.getChildren().add(group);
            this.buildingMap.put(buildingInterface.getType(), buildingInterface);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private String tooltip(BuildingInterface buildingInterface) {
        return buildingInterface.getName() + "\n\n" +
                "Kosten: " + buildingInterface.getCosts() + "\n" +
                "Einkommen: " + buildingInterface.getIncome() + "\n";
    }

    public void updateList() {
        this.buildingMap.forEach((type, building) -> this.updateBuildingStatus(building));
    }

    private void updateBuildingStatus(BuildingInterface buildingInterface) {
        Boolean isActive = buildingInterface.getCosts() <= this.player.getScore();

        if(buildingInterface.getStatus() != isActive) {
            Group group = buildingInterface.getGroup();
            Pane imageWrapper = buildingInterface.getImageWrapper();

            if(!isActive) {
                group.getStyleClass().add("not-enough-money");
                imageWrapper.getStyleClass().remove("selectable");
            } else {
                group.getStyleClass().remove("not-enough-money");
                imageWrapper.getStyleClass().add("selectable");
            }
        }

        buildingInterface.setStatus(isActive);
    }

    private void assignClickedBuildingToActive(Object clicked) {
        this.buildingMap.forEach((type, building) -> {
            if(building.getGroup().equals(clicked) && building.getStatus()) {

                if(this.activeBuilding == null) {
                    this.setActiveBuilding(building);
                } else {
                    if(clicked.equals(this.activeBuilding.getGroup())) {
                        this.setActiveBuilding(null);
                    } else {
                        this.setActiveBuilding(null);
                        this.setActiveBuilding(building);
                    }
                }
            }
        });
    }
}
