package main.beuthcity.buildings.exceptions;

public class NotSpotOwnerException extends Exception {
    public NotSpotOwnerException() {}

    public NotSpotOwnerException(String message) {
        super(message);
    }
}
