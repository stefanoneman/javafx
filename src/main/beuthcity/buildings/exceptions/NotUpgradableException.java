package main.beuthcity.buildings.exceptions;

public class NotUpgradableException extends Exception {
    public NotUpgradableException() {}

    public NotUpgradableException(String message) {
        super(message);
    }
}
