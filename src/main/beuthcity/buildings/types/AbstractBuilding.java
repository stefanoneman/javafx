package main.beuthcity.buildings.types;

import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

import java.io.FileNotFoundException;

public abstract class AbstractBuilding {
    protected Type type;
    private Group group;
    private Boolean status = null;

    public abstract String getName();
    public abstract Integer getCosts();
    public abstract Integer getIncome();
    public abstract ImageView getImageView() throws FileNotFoundException;
    public abstract Type getType();

    public Boolean getStatus() {
        return this.status;
    }

    public Group getGroup() {
        return this.group;
    }

    public Pane getImageWrapper() {
        return (Pane)this.group.getChildren().get(0);
    }

    public Image getImage() throws FileNotFoundException {
        return this.getImageView().getImage();
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
