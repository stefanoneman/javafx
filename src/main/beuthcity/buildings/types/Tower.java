package main.beuthcity.buildings.types;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Tower extends AbstractBuilding implements BuildingInterface {
    @Override
    public String getName() {
        return "Stadthaus";
    }

    @Override
    public Integer getCosts() {
        return 1100;
    }

    @Override
    public Integer getIncome() {
        return 125;
    }

    @Override
    public ImageView getImageView() throws FileNotFoundException {
        return new ImageView(new Image(new FileInputStream(new File("resources/images/buildings/building-4.png"))));
    }

    @Override
    public Type getType() {
        return Type.TOWER;
    }

    @Override
    public Type getUpgrade() {
        return Type.SKYSCRAPER;
    }
}
