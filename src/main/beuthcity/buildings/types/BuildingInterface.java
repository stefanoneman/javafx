package main.beuthcity.buildings.types;

import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

import java.io.FileNotFoundException;

public interface BuildingInterface {
    Integer getCosts();
    Integer getIncome();
    String getName();
    Image getImage() throws FileNotFoundException;
    ImageView getImageView() throws FileNotFoundException;
    Boolean getStatus();
    Group getGroup();
    Pane getImageWrapper();
    Type getType();
    Type getUpgrade();

    void setStatus(Boolean status);
    void setGroup(Group group);
}