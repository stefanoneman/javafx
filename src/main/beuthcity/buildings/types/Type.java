package main.beuthcity.buildings.types;

public enum Type {
    NONE,
    HOUSE,
    LODGE,
    LOFT,
    TOWER,
    SKYSCRAPER
}
