package main.beuthcity.buildings.types;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Loft extends AbstractBuilding implements BuildingInterface {
    @Override
    public String getName() {
        return "Loft-Wohnung";
    }

    @Override
    public Integer getCosts() {
        return 450;
    }

    @Override
    public Integer getIncome() {
        return 50;
    }

    @Override
    public ImageView getImageView() throws FileNotFoundException {
        return new ImageView(new Image(new FileInputStream(new File("resources/images/buildings/building-3.png"))));
    }

    @Override
    public Type getType() {
        return Type.LOFT;
    }

    @Override
    public Type getUpgrade() {
        return Type.TOWER;
    }
}
