package main.beuthcity.buildings.types;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class House extends AbstractBuilding implements BuildingInterface {
    @Override
    public String getName() {
        return "einfaches Haus";
    }

    @Override
    public Integer getCosts() {
        return 100;
    }

    @Override
    public Integer getIncome() {
        return 20;
    }

    @Override
    public ImageView getImageView() throws FileNotFoundException {
        return new ImageView(new Image(new FileInputStream(new File("resources/images/buildings/building-1.png"))));
    }

    @Override
    public Type getType() {
        return Type.HOUSE;
    }

    @Override
    public Type getUpgrade() {
        return Type.LODGE;
    }
}
