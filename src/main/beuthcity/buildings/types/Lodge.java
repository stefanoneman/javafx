package main.beuthcity.buildings.types;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Lodge extends AbstractBuilding implements BuildingInterface {
    @Override
    public String getName() {
        return "Landhaus";
    }

    @Override
    public Integer getCosts() {
        return 220;
    }

    @Override
    public Integer getIncome() {
        return 35;
    }

    @Override
    public ImageView getImageView() throws FileNotFoundException {
        return new ImageView(new Image(new FileInputStream(new File("resources/images/buildings/building-2.png"))));
    }

    @Override
    public Type getType() {
        return Type.LODGE;
    }

    @Override
    public Type getUpgrade() {
        return Type.LOFT;
    }
}
