package main.beuthcity;


import javafx.scene.control.TextArea;

public class MessageRelay {
    private static MessageRelay instance = null;
    private TextArea textArea;

    private MessageRelay(TextArea textArea) {
        this.textArea = textArea;
    }

    public synchronized void addMessage(String message) {
        textArea.appendText("\n" + message);
    }

    public synchronized void addMessage(String message, String playerName, Integer playerScore) {
        textArea.appendText("\nNachricht von Spieler `" + playerName + "` (Punktestand: " + playerScore + "): " + message);
    }

    public static MessageRelay createInstance(TextArea textArea){
        if (instance != null){
            throw new IllegalStateException("createInstance can only be called once");
        }

        instance = new MessageRelay(textArea);
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 20; i++) {
            stringBuilder.append('\n');
        }
        instance.addMessage(stringBuilder.toString());

        return instance;
    }

    public static MessageRelay getInstance(){
        if(instance == null){
            throw new IllegalStateException("createInstance(TextArea) must be called first.");
        }
        return instance;
    }
}