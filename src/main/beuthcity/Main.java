package main.beuthcity;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.TilePane;
import javafx.stage.Stage;

import static javafx.application.Platform.exit;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = new FXMLLoader().load(getClass().getResource("/main.fxml"));
        root.getStylesheets().add("/css/main.css");

        if (!(root instanceof BorderPane)) {
            throw new IllegalStateException("Wrong FXML or wrong version");
        }

        BorderPane borderPane = (BorderPane) root;
        if (!(borderPane.getCenter() instanceof TilePane)) {
            throw new IllegalStateException("Wrong FXML or wrong version");
        }

        primaryStage.setTitle("");
        primaryStage.setScene(new Scene(borderPane, borderPane.getPrefWidth(), borderPane.getPrefHeight()));
        primaryStage.setResizable(false);
        primaryStage.show();
        primaryStage.setOnCloseRequest(event -> {
            exit();
            System.exit(0);
        });
    }


    public static void main(String[] args) {
        launch(args);
    }
}