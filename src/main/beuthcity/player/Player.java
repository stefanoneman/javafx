package main.beuthcity.player;

import main.beuthcity.buildings.types.BuildingInterface;

public class Player {
    private Integer score = 200;
    private Integer income = 0;
    private String name;
    private Integer port;

    public Player() { }
    public Player(String name, Integer port, Integer score) {
        this.name = name;
        this.port = port;
        this.score = score;
    }

    public Integer getScore() {
        return this.score;
    }

    public Integer getIncome() {
        return this.income;
    }

    public String getName() {
        return this.name;
    }

    public Integer getPort() {
        return this.port;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public void increaseScoreByIncome() {
        this.score += this.income;
    }

    public void updateScoreAndIncome(BuildingInterface buildingInterface) {
        this.score -= buildingInterface.getCosts();
        this.income += buildingInterface.getIncome();
    }

    @Override
    public String toString() {
        return this.name + " (" + this.score + ")";
    }
}
