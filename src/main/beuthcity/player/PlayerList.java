package main.beuthcity.player;


import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;

import java.util.Optional;

public class PlayerList {
    private final ListView<Player> playerListView;
    private ObservableList<Player> playerList = FXCollections.observableArrayList();

    public PlayerList(ListView<Player> playerListView) {
        this.playerListView = playerListView;
        this.update();
    }

    public void addPlayer(Player player) {
        this.playerList.add(player);
        this.update();
    }

    public Player getPlayerByPort(Integer port) {
        return this.playerList.stream().filter(player -> player.getPort().equals(port)).findFirst().orElse(null);
    }

    public void removePlayerByPort(Integer port) {
        Player player = this.getPlayerByPort(port);
        this.playerList.remove(player);
        this.update();
    }

    public void removeAll() {
        this.playerList = FXCollections.observableArrayList();
        this.update();
    }

    public void update() {
        Platform.runLater(() -> {
            this.playerListView.setItems(null);
            this.playerListView.setItems(this.playerList);
        });
    }
}
