package main.beuthcity.network.proxy;

import main.beuthcity.network.socket.SocketInterface;
import main.beuthcity.network.socket.objects.SocketObject;

import java.io.IOException;

public class Proxy {
    private static Proxy instance = new Proxy();
    private SocketInterface socketInterface;

    private Proxy() { }

    public static Proxy getInstance(){
        if(instance == null){
            throw new IllegalStateException("createInstance() must be called first.");
        }
        return instance;
    }

    public synchronized SocketInterface getSocketInterface() {
        return this.socketInterface;
    }

    public synchronized void setSocketInterface(SocketInterface socketInterface) {
        this.socketInterface = socketInterface;
    }

    public synchronized void send(SocketObject socketObject) {
        try {
            if(this.socketInterface != null) {
                this.socketInterface.send(socketObject);
            }
        } catch (IOException ignored) {

        }
    }
}
