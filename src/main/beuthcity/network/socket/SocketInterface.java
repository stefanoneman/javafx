package main.beuthcity.network.socket;

import main.beuthcity.network.socket.objects.SocketObject;

import java.beans.PropertyChangeListener;
import java.io.IOException;

public interface SocketInterface {
    Integer getPort();
    void send(SocketObject socketObject) throws IOException;

    void addPropertyChangeListener(PropertyChangeListener pcl);
    void removePropertyChangeListener(PropertyChangeListener pcl);
    void update(SocketObject socketObject);
}
