package main.beuthcity.network.socket;

import main.beuthcity.network.socket.objects.SocketObject;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public abstract class SocketObservable {
    private SocketObject socketObject;
    private PropertyChangeSupport support;

    SocketObservable() {
        this.support = new PropertyChangeSupport(this);
    }

    public void addPropertyChangeListener(PropertyChangeListener pcl) {
        this.support.addPropertyChangeListener(pcl);
    }

    public void removePropertyChangeListener(PropertyChangeListener pcl) {
        this.support.removePropertyChangeListener(pcl);
    }

    public void update(SocketObject socketObject) {
        support.firePropertyChange("update", this.socketObject, socketObject);
        this.socketObject = socketObject;
    }
}
