package main.beuthcity.network.socket;

import main.beuthcity.MessageRelay;
import main.beuthcity.player.Player;
import main.beuthcity.network.socket.objects.SocketObject;
import main.beuthcity.network.socket.objects.SocketPlayer;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.net.SocketException;

public class Client extends SocketObservable implements SocketInterface, Runnable {
    private Player player;
    private Socket socket;
    private ObjectOutputStream out;

    public Client(Player player, String ip, Integer port) throws Exception {
        try {
            this.player = player;
            this.socket = new Socket(ip, port);
            System.out.println("Connected: " + this.socket);

            this.out = new ObjectOutputStream(this.socket.getOutputStream());
        } catch (ConnectException e) {
            MessageRelay.getInstance().addMessage("Es war nicht möglich zum Server zu verbinden.");
            throw new Exception("unable to connect to server");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Integer getPort() {
        return this.socket.getLocalPort();
    }

    @Override
    public void run() {
        try {
            String name = this.player.getName();
            Integer port = this.socket.getLocalPort();
            Integer score = this.player.getScore();
            SocketPlayer.Status status = SocketPlayer.Status.CONNECTED;

            this.send(new SocketPlayer("Neuer Spieler: " + name, name, port, score, status));
            this.listen();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void listen() throws Exception {
        try {
            ObjectInputStream in = new ObjectInputStream(this.socket.getInputStream());
            do {
                SocketObject socketObject = (SocketObject) in.readObject();
                this.update(socketObject);
            } while (true);
        } catch (SocketException e) {
            MessageRelay.getInstance().addMessage("Verbindung wurde vom Server terminiert.");
            throw new Exception(e.getMessage());
        } catch (EOFException ignored) {

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void send(SocketObject socketObject) throws IOException {
        if(socketObject.getMessage() != null) {
            MessageRelay.getInstance().addMessage(socketObject.getMessage());
        }

        this.out.writeObject(socketObject);
        this.out.flush();
    }
}
