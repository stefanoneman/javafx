package main.beuthcity.network.socket;

import main.beuthcity.MessageRelay;
import main.beuthcity.gamemap.GameMap;
import main.beuthcity.network.socket.objects.SocketGameMap;
import main.beuthcity.network.socket.objects.SocketObject;
import main.beuthcity.network.socket.objects.SocketPlayer;
import main.beuthcity.player.Player;
import main.beuthcity.player.PlayerList;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class Server extends SocketObservable implements SocketInterface, Runnable {
    private ServerSocket serverSocket;
    private GameMap gameMap;
    private Player player;
    private PlayerList playerList;

    private ObjectOutputStream out;

    public Server(Integer port, GameMap gameMap, Player player, PlayerList playerList) throws Exception {
        try {
            this.serverSocket = new ServerSocket(port);
            this.gameMap = gameMap;
            this.player = player;
            this.playerList = playerList;

            MessageRelay.getInstance().addMessage("Server wird erstellt.");
        } catch (BindException e) {
            MessageRelay.getInstance().addMessage("Server konnte nicht erstellt werden.");
            throw new Exception("unable to create server");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Integer getPort() {
        return this.serverSocket.getLocalPort();
    }

    @Override
    public void run() {
        MessageRelay.getInstance().addMessage("Warte auf Mitspieler...");
        this.listen();
    }

    private void listen() {
        Socket clientSocket = null;

        try {
            clientSocket = serverSocket.accept();
            System.out.println("Connected: " + clientSocket);

            this.out = new ObjectOutputStream(clientSocket.getOutputStream());

            String name = this.player.getName();
            Integer port = this.player.getPort();
            Integer score = this.player.getScore();
            SocketPlayer.Status status = SocketPlayer.Status.CONNECTED;

            this.send(new SocketPlayer("Erfolgreich mit Server " + serverSocket.getLocalSocketAddress() + " verbunden.", name, port, score, status));
            this.send(new SocketGameMap("Lade Spiel...", this.gameMap.getCurrentGameMap()));

            ObjectInputStream in = new ObjectInputStream(clientSocket.getInputStream());
            do {
                SocketObject socketObject = (SocketObject) in.readObject();
                this.update(socketObject);
            } while (true);
        } catch(EOFException | SocketException ignored) {

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                assert clientSocket != null;
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Player player = this.playerList.getPlayerByPort(clientSocket.getPort());
            if(player != null) {
                String name = player.getName();
                Integer port = player.getPort();
                Integer score = player.getScore();
                SocketPlayer.Status status = SocketPlayer.Status.DISCONNECTED;
                this.update(new SocketPlayer("Spieler hat das Spiel verlassen.", name, port, score, status));
                System.out.println("Closed: " + clientSocket);
            }

            this.gameMap.cleanup();
            this.run();
        }
    }

    @Override
    public void send(SocketObject socketObject) throws IOException {
        if(this.out != null) {
            if(socketObject.getMessage() != null) {
                MessageRelay.getInstance().addMessage(socketObject.getMessage());
            }
            this.out.writeObject(socketObject);
            this.out.flush();
        }
    }
}