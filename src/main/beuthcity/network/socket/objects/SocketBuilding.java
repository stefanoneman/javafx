package main.beuthcity.network.socket.objects;

import main.beuthcity.buildings.types.Type;

import java.io.Serializable;

public class SocketBuilding extends AbstractSocketObject implements SocketObject, Serializable {
    private Integer spotIndex;
    private String name;
    private Integer score;
    private Type building;

    public SocketBuilding(String message, Integer spotIndex, String name, Integer score, Type building) {
        super(message);

        this.spotIndex = spotIndex;
        this.name = name;
        this.score = score;
        this.building = building;
    }

    public Integer getSpotIndex() {
        return this.spotIndex;
    }

    public String getName() {
        return this.name;
    }

    public Integer getScore() {
        return this.score;
    }

    public Type getBuilding() {
        return this.building;
    }
}
