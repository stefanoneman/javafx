package main.beuthcity.network.socket.objects;

import java.io.Serializable;

public class SocketMessage extends AbstractSocketObject implements SocketObject, Serializable {
    public SocketMessage(String message) {
        super(message);
    }
}