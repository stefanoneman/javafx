package main.beuthcity.network.socket.objects;

import main.beuthcity.buildings.types.Type;

import java.io.Serializable;
import java.util.List;

public class SocketGameMap extends AbstractSocketObject implements SocketObject, Serializable {
    private List<Type> gameMap;

    public SocketGameMap(String message, List<Type> gameMap) {
        super(message);

        this.gameMap = gameMap;
    }

    public List getGameMap() {
        return this.gameMap;
    }
}
