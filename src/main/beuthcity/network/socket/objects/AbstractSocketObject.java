package main.beuthcity.network.socket.objects;

import java.io.Serializable;

public class AbstractSocketObject implements Serializable {
    String message;

    AbstractSocketObject(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}
