package main.beuthcity.network.socket.objects;

import java.io.Serializable;

public class SocketPlayer extends AbstractSocketObject implements SocketObject, Serializable {
    private String name;
    private Integer port;
    private Integer score;
    private Status status;

    public enum Status {
        CONNECTED,
        DISCONNECTED,
        UPDATE
    }

    public SocketPlayer(String message, String name, Integer port, Integer score, Status status) {
        super(message);

        this.name = name;
        this.port = port;
        this.score = score;
        this.status = status;
    }

    public String getName() {
        return this.name;
    }

    public Integer getPort() {
        return this.port;
    }

    public Integer getScore() {
        return this.score;
    }

    public Status getStatus() {
        return this.status;
    }
}
