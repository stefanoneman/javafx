package main.beuthcity.gamemap;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;
import main.beuthcity.Controller;
import main.beuthcity.MessageRelay;
import main.beuthcity.buildings.BuildingMap;
import main.beuthcity.buildings.exceptions.NotSpotOwnerException;
import main.beuthcity.buildings.exceptions.NotUpgradableException;
import main.beuthcity.buildings.types.BuildingInterface;
import main.beuthcity.network.proxy.Proxy;
import main.beuthcity.network.socket.objects.*;
import main.beuthcity.player.Player;
import main.beuthcity.buildings.types.Type;
import main.beuthcity.player.PlayerList;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.FileNotFoundException;
import java.io.NotActiveException;
import java.util.ArrayList;
import java.util.List;

public class GameMap implements PropertyChangeListener {
    private final TilePane root;
    private Controller controller;
    private BuildingMap buildingMap;
    private Player player;
    private PlayerList playerList;
    private Proxy proxy;

    private Boolean active = false;
    private ObservableList<Spot> gameMap = FXCollections.observableArrayList();

    public GameMap(TilePane root,
                   Controller controller,
                   BuildingMap buildingMap,
                   Player player,
                   PlayerList playerList,
                   Proxy proxy
    ) {
        this.root = root;
        this.controller = controller;
        this.buildingMap = buildingMap;
        this.player = player;
        this.playerList = playerList;
        this.proxy = proxy;

        this.setGameMap();
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getActive() {
        return this.active;
    }

    private void setGameMap() {
        for(int i = 0; i < 40; i++) {
            this.generate();
        }
    }

    private void generate() {
        Spot spot = new Spot(this);
        this.root.getChildren().add(spot.getPane());
        this.gameMap.add(spot);
    }

    public void cleanup() {
        Platform.runLater(() -> {
            int i = 0;
            for(Spot spot : this.gameMap) {
                if(spot.getOwner() != this.player) {
                    spot = new Spot(this);
                    this.root.getChildren().set(i, spot.getPane());
                    this.gameMap.set(i, spot);
                }

                i++;
            }
        });
    }

    public void update() {
        this.controller.updateScoreAndIncome();
        this.buildingMap.updateList();
    }

    private void update(BuildingInterface buildingInterface) {
        this.player.updateScoreAndIncome(buildingInterface);
        this.controller.updateScoreAndIncome();
        this.buildingMap.updateList();
    }

    public void assignBuildingToClickedSpot(Object clicked) {
        try {
            if(!this.active) {
                throw new NotActiveException("Das Spiel wurde noch nicht gestartet");
            }

            BuildingInterface activeBuildingInterface = this.buildingMap.getActiveBuilding();

            if(activeBuildingInterface != null) {
                this.gameMap.forEach((spot) -> {
                    if(spot.getPane().equals(clicked)) {
                        try {
                            spot.build(activeBuildingInterface, this.player);
                            this.update(activeBuildingInterface);
                            this.buildingMap.setActiveBuilding(null);

                            String message = "Neues Gebäude [" + activeBuildingInterface.getName() + "] gebaut";
                            Integer spotIndex = this.gameMap.indexOf(spot);
                            String playerName = this.player.getName();
                            Integer playerScore = this.player.getScore();
                            Type type = activeBuildingInterface.getType();

                            this.proxy.send(new SocketBuilding(message, spotIndex, playerName, playerScore, type));
                        } catch (NotSpotOwnerException | NotUpgradableException e) {
                            MessageRelay.getInstance().addMessage(e.getMessage());
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        } catch(NotActiveException e) {
            MessageRelay.getInstance().addMessage(e.getMessage());
        }
    }

    private void assignBuildingToReceivedSpot(Integer spotTarget, BuildingInterface buildingInterface) {
        this.gameMap.forEach((spot) -> {
            if(this.gameMap.indexOf(spot) == spotTarget && buildingInterface != null) {
                try {
                    spot.build(buildingInterface, null);
                } catch (NotSpotOwnerException | NotUpgradableException e) {
                    MessageRelay.getInstance().addMessage(e.getMessage());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        Platform.runLater(() -> {
            SocketObject socketObject = (SocketObject)propertyChangeEvent.getNewValue();
            if(socketObject instanceof SocketPlayer) {
                String name = ((SocketPlayer) socketObject).getName();
                Integer port = ((SocketPlayer) socketObject).getPort();
                Integer score = ((SocketPlayer) socketObject).getScore();
                SocketPlayer.Status status = ((SocketPlayer) socketObject).getStatus();

                if(status == SocketPlayer.Status.CONNECTED) {
                    this.playerList.addPlayer(new Player(name, port, score));
                    MessageRelay.getInstance().addMessage(socketObject.getMessage(), name, score);
                } else if(status == SocketPlayer.Status.DISCONNECTED){
                    this.playerList.removePlayerByPort(port);
                    MessageRelay.getInstance().addMessage(socketObject.getMessage(), name, score);
                    MessageRelay.getInstance().addMessage("Warte auf Mitspieler...");
                } else if(status == SocketPlayer.Status.UPDATE) {
                    Player player = this.playerList.getPlayerByPort(port);
                    if(player != null) {
                        player.setScore(score);
                        this.playerList.update();
                    }
                }

            } else if(socketObject instanceof SocketBuilding) {
                String name = ((SocketBuilding) socketObject).getName();
                Integer score = ((SocketBuilding) socketObject).getScore();
                Integer spotIndex = ((SocketBuilding) socketObject).getSpotIndex();
                Type type = ((SocketBuilding) socketObject).getBuilding();
                this.assignBuildingToReceivedSpot(spotIndex, this.buildingMap.getBuildingByType(type));
                MessageRelay.getInstance().addMessage(socketObject.getMessage(), name, score);
            } else if (socketObject instanceof SocketGameMap) {
                List gameMap = ((SocketGameMap) socketObject).getGameMap();
                for(int spotIndex = 0; spotIndex < gameMap.size(); spotIndex++) {
                    this.assignBuildingToReceivedSpot(spotIndex, this.buildingMap.getBuildingByType((Type)gameMap.get(spotIndex)));
                }
            } else if(socketObject instanceof SocketMessage) {
                MessageRelay.getInstance().addMessage(socketObject.getMessage());
            }
        });
    }

    public List<Type> getCurrentGameMap() {
        List<Type> gameMap = new ArrayList<>();
        this.gameMap.forEach((spot) -> {
            BuildingInterface buildingInterface = spot.getBuilding();
            if(buildingInterface != null) {
                gameMap.add(buildingInterface.getType());
            } else {
                gameMap.add(Type.NONE);
            }
        });

        return gameMap;
    }
}