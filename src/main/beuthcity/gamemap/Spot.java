package main.beuthcity.gamemap;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import main.beuthcity.buildings.exceptions.NotSpotOwnerException;
import main.beuthcity.buildings.exceptions.NotUpgradableException;
import main.beuthcity.player.Player;
import main.beuthcity.buildings.types.BuildingInterface;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Spot {
    private ImageView image;

    {
        try {
            image = new ImageView(new Image(new FileInputStream(new File("resources/images/placeholder.png"))));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Pane pane;
    private BuildingInterface building;
    private Player owner;

    Spot(GameMap gameMap) {
        Pane imageWrapper = new Pane(this.image);
        imageWrapper.getStyleClass().addAll("buildingWrapper", "selectable");

        Pane pane = new Pane(imageWrapper);
        pane.onMouseClickedProperty().set(event -> gameMap.assignBuildingToClickedSpot(event.getSource()));

        this.pane = pane;
    }

    public Image getImage() {
        return this.image.getImage();
    }

    public ImageView getImageView() {
        return this.image;
    }

    Pane getPane() {
        return this.pane;
    }

    BuildingInterface getBuilding() {
        return this.building;
    }

    Player getOwner() {
        return this.owner;
    }

    void setPane(Pane pane) {
        this.pane = pane;
    }

    void build(BuildingInterface building, Player player) throws NotSpotOwnerException, NotUpgradableException, FileNotFoundException {
        if(this.building != null && this.owner != player) {
            throw new NotSpotOwnerException("FEHLER: Auf diesem Grundstück hat bereits ein anderer Spieler gebaut!");
        } else if(this.building != null && this.building.getUpgrade() != building.getType()) {
            throw new NotUpgradableException("FEHLER: Das Gebäude kann nicht erweitert werden, weil es nicht die nächst höhere Stufe ist.");
        }

        this.building = building;
        this.image.setImage(building.getImage());
        this.owner = player;
        this.pane.getStyleClass().add(player != null ? "blue" : "red");
    }

    public String toString() {
        return getClass().getSimpleName() +
                "[image=" + this.image + "]" +
                "[pane=" + this.pane + "]" +
                "[building=" + this.building + "]";
    }
}
