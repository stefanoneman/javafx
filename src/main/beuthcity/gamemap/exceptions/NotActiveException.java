package main.beuthcity.gamemap.exceptions;

public class NotActiveException extends Exception {
    public NotActiveException() {}

    public NotActiveException(String message) {
        super(message);
    }
}
