package main.beuthcity;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.TilePane;
import main.beuthcity.buildings.BuildingMap;
import main.beuthcity.network.socket.Client;
import main.beuthcity.network.socket.Server;
import main.beuthcity.gamemap.GameMap;
import main.beuthcity.network.proxy.Proxy;
import main.beuthcity.network.socket.SocketInterface;
import main.beuthcity.network.socket.objects.SocketPlayer;
import main.beuthcity.player.Player;
import main.beuthcity.player.PlayerList;

import java.util.regex.Pattern;

public class Controller {
    @FXML
    public TextArea console;
    @FXML
    public TextField ip;
    @FXML
    public TextField port;
    @FXML
    public ToggleGroup type;
    @FXML
    public RadioButton start;
    @FXML
    public RadioButton join;
    @FXML
    public TextField playerName;
    @FXML
    public Button startGame;
    @FXML
    public TextField score;
    @FXML
    public TextField income;
    @FXML
    public TilePane buildingMapTilePane;
    @FXML
    public TilePane gameMapTilePane;
    @FXML
    public ListView<Player> playerListView;

    private Proxy proxy;

    private Player player;

    private PlayerList playerList;

    private GameMap gameMap;

    @FXML
    void initialize() {
        MessageRelay.createInstance(console);
        this.proxy = Proxy.getInstance();
        this.player = new Player();
        this.playerList = new PlayerList(this.playerListView);
        this.updateScoreAndIncome();

        BuildingMap buildingMap = new BuildingMap(this.buildingMapTilePane, this.player);
        this.gameMap = new GameMap(
                this.gameMapTilePane,
                this,
                buildingMap,
                this.player,
                this.playerList,
                this.proxy
        );

        Thread thread = new Thread(() -> {
           while(true) {
               try {
                   this.player.increaseScoreByIncome();
                   this.playerList.update();
                   this.gameMap.update();
                   if(this.gameMap.getActive()) {
                       String name = this.player.getName();
                       Integer port = this.proxy.getSocketInterface().getPort();
                       Integer score = this.player.getScore();
                       SocketPlayer.Status status = SocketPlayer.Status.UPDATE;

                       this.proxy.send(new SocketPlayer(null, name, port, score, status));
                   }

                   Thread.sleep(2000);
               } catch (InterruptedException e) {
                   return;
               }
           }
        });
        thread.setDaemon(true);
        thread.start();

        Pattern numeric = Pattern.compile("\\d+");
        port.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!numeric.matcher(newValue).matches()) {
                port.setText(oldValue);
            }
        });
    }

    private String getPlayerName() {
        return this.playerName.getText();
    }

    private String getIP() {
        return this.ip.getText();
    }

    private Integer getPort() {
        return Integer.parseInt(this.port.getText());
    }

    private void setScore(Integer score) {
        this.score.setText(score.toString());
    }

    private void setIncome(Integer income) {
        this.income.setText(income.toString());
    }

    public void updateScoreAndIncome() {
        this.setScore(this.player.getScore());
        this.setIncome(this.player.getIncome());
    }

    @FXML
    public void onAction() {
        try {
            this.player.setName(this.getPlayerName());
            this.playerList.removeAll();

            SocketInterface socketInterface;
            if (type.getSelectedToggle() == start) {
                socketInterface = new Server(this.getPort(), this.gameMap, this.player, this.playerList);
            } else {
                socketInterface = new Client(this.player, this.getIP(), this.getPort());
            }

            this.playerList.addPlayer(this.player);
            this.gameMap.setActive(true);

            Thread thread = new Thread((Runnable)socketInterface);
            thread.setDaemon(true);
            thread.start();

            this.proxy.setSocketInterface(socketInterface);
            socketInterface.addPropertyChangeListener(this.gameMap);

            this.player.setPort(this.proxy.getSocketInterface().getPort());
            this.playerName.setDisable(true);
            this.ip.setDisable(true);
            this.port.setDisable(true);
            this.ip.setDisable(true);
            this.start.setDisable(true);
            this.join.setDisable(true);
            this.startGame.setDisable(true);
        } catch (Exception e) {
            this.gameMap.setActive(false);
            System.out.println(e.getMessage());
        }
    }
}