#FHB-MIB13-S19-Patterns&Frameworks (Beuthcity)

This small game is like the famous SimCity-Game.
Two players can play against each other and create new Buildings, depending on their Income and Score.
Buildings can be upgraded, to increase your Income.

## Build & Run

### Build
To build the application, please use IntelliJ's `beuthcity.iml`, located at the root-folder.
This will compile all mandatory java- and resource-files and move them to `out/`.

### Run
It  is mandatory to have these `VM options` on your run-script:
```
--module-path
$ContentRoot$:lib/javafx
--add-modules
javafx.controls,javafx.fxml
```

Otherwise the application won't start.
You can run the application alternatively by running `run.sh`

## Project Structure

    .
    ├── out         # Compiled files
    ├── docs        # Documentation files
    ├── src         # Source files
    ├── resources   # Files, like Images, FXML, ...
    ├── lib         # JavaFX-Library-Files
    ├── beuthcity.iml
    └── README.md

##Author
Patrick Rauchfuss

